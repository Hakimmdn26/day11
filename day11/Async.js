import { View, Text, Touchable, TouchableOpacity, TextInput } from 'react-native';
import React from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';

const App =  () => {
  const [AIB, setAIB] = useState('');
  const [tampil, setTampil] = useState('');
  const Simpan = async {value: any} => {
// get Item ambil data
// set Item buat data atau simpan data
// remove Item untuk hapus data per Item
// clear hapus semua data atau hapus chat kenangan foto dll
  await AsyncStorage.setItem('AIB', value);
};


const ambil = async () => {
  try {
    const data = await AsyncStorage.getItem('AIB'); 
    console.log(data);
    setTampil(data);
  } catch (e) {
    console.log(e);
    }
  };

const removes = async () => {
  try{
    await AsyncStorage.removeItem('AIB');
    ambil();
  } catch (e) {
    console.log(e);
  }
};

  return (
    <View>
      <TextInput
      placeholder="CERITAKAN AIB MU"
      value={AIB}
      onChangeText={E => {
        setAIB(E);
      }}
      />
      <TouchableOpacity
      onPress={() => {
        Simpan(AIB);
      }}>
      <Text>Konfirmasi</Text>
      </TouchableOpacity>

      <Text>{tampil}</Text>
      <TouchableOpacity
      onPress={() => {
        ambil();
      }}>
        <Text>AMBIL DATA CUY</Text>
      </TouchableOpacity>

      <TouchableOpacity
        onPress={() => {
          removes();
        }}>
        <Text>REMOVE</Text>
      </TouchableOpacity>
    </View>
  );
};

export default App