import React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack'; // untuk membuat screen bisa di navigasi
import BottomNav from '../Component/BottomNav';
import Transaksi from '../App/Transaksi';
import {View} from 'react-native';
import Home from './home';
import Registrasi from '../App/Registrasi';

const Stack = createNativeStackNavigator();
const Route = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen name="BottomNav" component={BottomNav} />
      <Stack.Screen name="Registrasi" component={Registrasi} />
      {/* <Stack.Screen name="ProfiNavigator" component={ProfileNavigator} /> */}
      <Stack.Screen name="Transaksi" component={Transaksi} />
      {/* <Stack.Screen name="History" component={History} /> */}
      {/* <Stack.Screen name="GrabFood" component={GrabFood} /> */}
    </Stack.Navigator>
  );
};

export default Route;
