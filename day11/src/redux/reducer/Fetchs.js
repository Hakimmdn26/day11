const initialState = {
  DataFetchs: [],
  History: [],
};

const Fetchs = (state = initialState, action) => {
  switch (action.type) {
    case 'Home':
      return {
        ...state,
        DataFetchs: action.data,
      };
    case 'HISTORY':
      return {
        ...state,
        History: action.data,
      };
    default:
      return state;
  }
};

export default Fetchs;
