const initialState = {
  DataAuth: null,
};

const Auth = (state = initialState, action) => {
  switch (action.type) {
    case 'LOGIN':
      return {
        ...state,
        DataAuth: action.data,
      };
    case 'REGISTRASI':
      return {
        ...state,
        DataAuth: action.data,
      };
    default:
      return state;
  }
};

export default Auth;
