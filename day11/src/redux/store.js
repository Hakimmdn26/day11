import {createStore , applyMiddleware} from 'redux';
import rootReducer from './reducer/Index';
import thunk from 'redux-thunk';

// 1. kita membuat sebuah store dimana akan menyimpan data sebagai middleware atau penengah

const storeRedux = createStore(rootReducer,applyMiddleware(thunk));

// additional 2. kalau mau pake saga sagaMiddleware (rootsaga)

export default storeRedux;