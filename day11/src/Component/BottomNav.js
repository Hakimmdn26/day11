import {View, Text, Image} from 'react-native';
import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import History from '../App/History';
import Transaksi from '../App/Transaksi';
import Registrasi from '../App/Registrasi';
import ProfileNavigator from '../App/ProfileNavigator';

const Tab = createBottomTabNavigator();
const BottomNav = () => {
  return (
    <Tab.Navigator
      screenOptions={{
        headerShown: false,
        tabBarShowLabel: false, //untuk menghilangkan label di bottom tab biar bisa styling sendiri text labelnya
      }}>
      {/* tab screen untuk ngasih alamat yang ada di bottom tab biar bisa dipakai */}
      <Tab.Screen
        name="Transaksi"
        component={Transaksi}
        options={{
          tabBarIcon: ({size, color}) => (
            <View>
              <Text>Transaksi</Text>
            </View>
          ),
        }}
      />
      <Tab.Screen
        name="History"
        component={History}
        options={{
          tabBarIcon: ({size, color}) => (
            <View>
              <Text>History</Text>
            </View>
          ),
        }}
      />
      <Tab.Screen
        name="ProfileNavigator"
        component={ProfileNavigator}
        options={{
          tabBarIcon: ({size, color}) => (
            <View>
              <Text>Edit Profile</Text>
            </View>
          ),
        }}
      />
      <Tab.Screen
        name="Registrasi"
        component={Registrasi}
        options={{
          tabBarIcon: ({size, color}) => (
            <View>
              <Text>Registrasi</Text>
            </View>
          ),
        }}
      />
    </Tab.Navigator>
  );
};

export default BottomNav;
