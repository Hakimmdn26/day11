import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  SafeAreaView,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import ProfileInput from './Registrasi';

const ProfileNavigator = () => {
  const [profile, setProfile] = useState(null);
  const [showInput, setShowInput] = useState(false);

  const getProfile = async () => {
    const jsonValue = await AsyncStorage.getItem('@profile');
    setProfile(jsonValue != null ? JSON.parse(jsonValue) : null);
  };

  const saveProfile = async newProfile => {
    await AsyncStorage.setItem('@profile', JSON.stringify(newProfile));
    setProfile(newProfile);
    setShowInput(false);
  };

  useEffect(() => {
    getProfile();
    console.log(getProfile());
  }, []);

  return (
    <SafeAreaView style={styles.css.container}>
      <View>
        {showInput ? (
          <ProfileInput onSave={saveProfile} />
        ) : (
          <View>
            {profile ? (
              <View>
                <Text style={styles.css.nama}>Nama: {profile.name}</Text>
                <Text style={styles.css.umur}>Umur: {profile.age}</Text>
                <Text style={styles.css.email}>Email: {profile.email}</Text>
                <Text style={styles.css.nohp}>No. Hp: {profile.nohp}</Text>
                <Text style={styles.css.alamat}>Alamat: {profile.alamat}</Text>
                <TouchableOpacity
                  style={styles.css.edit}
                  onPress={() => setShowInput(true)}>
                  <Text
                    style={{
                      textAlign: 'center',
                      fontSize: 15,
                      marginTop: 20,
                      color: 'white',
                      fontWeight: 'bold',
                    }}>
                    Edit
                  </Text>
                </TouchableOpacity>
              </View>
            ) : (
              <TouchableOpacity
                style={styles.css.create}
                onPress={() => setShowInput(true)}>
                <Text
                  style={{
                    textAlign: 'center',
                    fontSize: 15,
                    marginTop: 20,
                    color: 'white',
                    fontWeight: 'bold',
                  }}>
                  Create Profile
                </Text>
              </TouchableOpacity>
            )}
          </View>
        )}
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  css: {
    container: {
      backgroundColor: '#6699ff',
      height: Platform.OS == 'android' ? 700 : 450,
      borderRadius: 20,
    },
    nama: {
      borderWidth: 0.5,
      overflow: 'hidden',
      borderColor: '#6699ff',
      borderRadius: 20,
      backgroundColor: 'white',
      fontSize: 18,
      marginTop: Platform.OS == 'android' ? 85 : 65,
      padding: 10,
      marginLeft: 10,
      marginRight: 10,
      color: 'black',
    },
    umur: {
      borderWidth: 0.5,
      borderColor: '#027588',
      overflow: 'hidden',
      backgroundColor: 'white',
      borderRadius: 20,
      fontSize: 18,
      marginTop: Platform.OS == 'android' ? 30 : 35,
      padding: 10,
      marginLeft: 10,
      marginRight: 10,
      color: 'black',
    },
    email: {
      borderWidth: 0.5,
      borderColor: '#027588',
      overflow: 'hidden',
      backgroundColor: 'white',
      borderRadius: 20,
      fontSize: 18,
      marginTop: Platform.OS == 'android' ? 30 : 35,
      padding: 10,
      marginLeft: 10,
      marginRight: 10,
      color: 'black',
    },
    nohp: {
      borderWidth: 0.5,
      borderColor: '#027588',
      overflow: 'hidden',
      backgroundColor: 'white',
      borderRadius: 20,
      fontSize: 18,
      marginTop: Platform.OS == 'android' ? 30 : 35,
      padding: 10,
      marginLeft: 10,
      marginRight: 10,
      color: 'black',
    },
    alamat: {
      borderWidth: 0.5,
      borderColor: '#027588',
      backgroundColor: 'white',
      overflow: 'hidden',
      borderRadius: 20,
      fontSize: 18,
      marginTop: Platform.OS == 'android' ? 30 : 35,
      padding: 10,
      marginLeft: 10,
      marginRight: 10,
      color: 'black',
    },
    edit: {
      textAlign: 'center',
      backgroundColor: '#0055ff',
      fontWeight: 'bold',
      alignItem: 'center',
      fontSize: 25,
      width: 200,
      height: 70,
      borderRadius: 45,
      marginTop: 30,
      marginLeft: 90,
    },
    create: {
      color: 'black',
      textAlign: 'center',
      backgroundColor: '#0055ff',
      fontWeight: 'bold',
      alignItem: 'center',
      fontSize: 25,
      width: 200,
      height: 70,
      borderRadius: 45,
      marginTop: 30,
      marginLeft: 90,
    },
  },
});

export default ProfileNavigator;
