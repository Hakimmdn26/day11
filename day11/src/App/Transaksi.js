import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  Platform,
  TextInput,
  KeyboardAvoidingView,
  ScrollView,
  Modal,
} from 'react-native';
import React, {useState} from 'react';
import axios from 'axios';

const Transaksi = () => {
  const [dataSender, setSender] = useState('');
  const [dataTarget, setTarget] = useState('');
  const [dataAmount, setAmount] = useState('');
  const [dataType, setType] = useState('');
  const [showModal, setShowModal] = useState(false);
  const [status, setStatus] = useState('');
  // useEffect(() => {
  //   send();
  // }, [amount, sender, receiver, status]);
  function send() {
    // var axios = require('axios');
    var data = JSON.stringify({
      amount: dataAmount,
      sender: dataSender,
      target: dataTarget,
      type: dataType,
    });

    var config = {
      method: 'post',
      maxBodyLength: Infinity,
      url: 'https://test-app-49a79-default-rtdb.asia-southeast1.firebasedatabase.app/transaction.json',
      headers: {
        'Content-Type': 'application/json',
      },
      data: data,
    };

    axios(config)
      .then(function (response) {
        console.log(JSON.stringify(response.data));
        setStatus('Transaksi berhasil!');
        handlePress();
      })
      .catch(function (error) {
        console.log(error);
        setStatus('Maaf, Transaksi anda Gagal');
        handlePress();
      });
  }
  const handlePress = () => {
    setShowModal(true);
  };

  return (
    <KeyboardAvoidingView style={{flex: 1}}>
      <SafeAreaView style={styles.canvas.container}>
        <ScrollView>
          <View style={styles.canvas.box}>
            <View style={styles.canvas.baratas}>
              <View style={styles.canvas.bartransfer}>
                <Text style={styles.text.mtransfer}>Kirim</Text>
              </View>
            </View>
            {/* // Penutup bar tengah */}
            <View>
              <View style={styles.canvas.boxtengahtransfer}>
                {/* // Penutup text bar */}
                <View>
                  <TextInput
                    placeholder="Pengirim"
                    keyboardType="default"
                    placeholderTextColor={'#027588'}
                    // fontFamily="Poppins-Regular"
                    style={styles.text.sender}
                    onChangeText={sender => {
                      setSender(sender);
                    }}
                  />
                </View>
                <View>
                  <TextInput
                    placeholder="Receiver/Alias"
                    keyboardType="default"
                    placeholderTextColor={'#027588'}
                    // fontFamily="Poppins-Regular"
                    style={styles.text.receiver}
                    onChangeText={receiver => {
                      setTarget(receiver);
                    }}
                  />
                </View>
                <View>
                  <TextInput
                    placeholder="Jumlah"
                    keyboardType="numeric"
                    placeholderTextColor={'#027588'}
                    // fontFamily="Poppins-Regular"
                    style={styles.text.amount}
                    onChangeText={amount => {
                      setAmount(amount);
                    }}
                  />
                </View>
                <View>
                  <TextInput
                    placeholder="Status"
                    keyboardType="default"
                    placeholderTextColor={'#027588'}
                    // fontFamily="Poppins-Regular"
                    style={styles.text.status}
                    onChangeText={status => {
                      setType(status);
                    }}
                  />
                </View>

                <View>
                  <TouchableOpacity onPress={send}>
                    <View style={styles.canvas.buttonsend}>
                      <Text style={styles.text.send}>Send</Text>
                    </View>
                  </TouchableOpacity>
                  <Modal
                    animationType="fade"
                    transparent={false}
                    visible={showModal}
                    onRequestClose={() => setShowModal(false)}>
                    <View
                      style={{
                        flex: 1,
                        backgroundColor: 'black',
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}>
                      <View
                        style={{
                          backgroundColor: '#0FAAF3',
                          padding: 95,
                          borderRadius: 10,
                        }}>
                        <Text
                          style={{
                            fontSize: 30,
                            fontWeight: 'bold',
                            marginBottom: 10,
                            textAlign: 'center',
                          }}>
                          Status
                        </Text>
                        <Text style={{fontSize: 16, color: 'white'}}>
                          {status}
                        </Text>
                        <Text style={{fontSize: 16}}>
                          Nama Pengirim : {dataSender}
                        </Text>
                        <Text style={{fontSize: 16}}>
                          Nama Penerima : {dataTarget}
                        </Text>
                        <Text style={{fontSize: 16}}>
                          Total : Rp. {dataAmount}
                        </Text>
                        <Text style={{fontSize: 16}}>Tipe : {dataType}</Text>
                        <TouchableOpacity onPress={() => setShowModal(false)}>
                          <Text
                            style={{
                              fontWeight: 'bold',
                              color: 'white',
                              marginTop: 10,
                              top: 70,
                              textAlign: 'center',
                              fontSize: 25,
                            }}>
                            Close
                          </Text>
                        </TouchableOpacity>
                      </View>
                    </View>
                  </Modal>
                </View>
              </View>
            </View>
            {/* // batas penutup view bar tengah */}
          </View>
        </ScrollView>
      </SafeAreaView>
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  canvas: {
    container: {backgroundColor: '#1b3c2e', flex: 1},

    box: {
      // backgroundColor: '#f1f3f3',
      height: Platform.OS == 'android' ? 580 : 690,

      borderRadius: 25,
      marginLeft: 10,
      marginRight: 10,
      marginTop: 10,
    },
    baratas: {
      // backgroundColor: 'grey',
      width: 360,
      height: 50,
      marginLeft: Platform.OS == 'android' ? 13 : 4,
      borderRadius: 10,
      alignItems: 'center',
      justifyContent: 'center',
      marginTop: 20,
    },
    bartransfer: {
      backgroundColor: '#8a948b',
      height: 35,
      width: 340,
      borderRadius: 10,
    },
    boxtengahtransfer: {
      backgroundColor: '#ebe8e1',
      width: Platform.OS == 'ios' ? 350 : 370,
      height: Platform.OS == 'ios' ? 350 : 370,
      borderRadius: 10,
      marginTop: 60,
      marginLeft: 11,
    },
    buttonsend: {
      backgroundColor: '#8a948b',
      width: 200,
      height: 70,
      marginTop: 30,
      marginLeft: 70,
      borderRadius: 45,
    },
  },
  text: {
    mtransfer: {
      color: 'black',
      fontWeight: 'bold',
      fontSize: 23,
      textAlign: 'center',
      // fontFamily: 'Poppins-Regular',
      marginTop: Platform.OS == 'ios' ? 3 : 3,
    },

    sender: {
      borderWidth: 0.5,
      borderColor: '#027588',
      borderRadius: 20,
      fontSize: 18,
      marginTop: Platform.OS == 'android' ? 30 : 35,
      padding: 10,
      marginLeft: 10,
      marginRight: 10,
      color: 'black',
    },

    receiver: {
      borderWidth: 0.5,
      borderColor: '#027588',
      borderRadius: 20,
      fontSize: 18,
      marginTop: 30,
      padding: 10,
      marginLeft: 10,
      marginRight: 10,
      color: 'black',
    },
    amount: {
      borderWidth: 0.5,
      borderColor: '#027588',
      borderRadius: 20,
      fontSize: 18,
      padding: 10,
      marginTop: 30,
      marginLeft: 10,
      marginRight: 10,
      color: 'black',
    },
    status: {
      borderColor: '#027588',
      borderWidth: 0.5,
      borderRadius: 20,
      fontSize: 18,
      padding: 10,
      marginTop: 30,
      marginLeft: 10,
      marginRight: 10,
      color: 'black',
    },
    send: {
      color: 'white',
      fontWeight: 'bold',
      marginTop: 18,
      fontSize: 25,
      // fontFamily: 'Poppins-Regular',
      textAlign: 'center',
    },
  },
});
export default Transaksi;
