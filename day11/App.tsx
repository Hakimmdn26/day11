import { NavigationContainer } from '@react-navigation/native';
import React from 'react';
import { View, Text } from 'react-native';
import {Provider} from 'react-redux';
import storeRedux  from './src/redux/store';
import Route from './src/Route/Route';


const App = () => {
  return (
   
      <Provider store={storeRedux}>
        <NavigationContainer>
        <Route/>
        </NavigationContainer>
      </Provider>
    
  );
};

export default App;